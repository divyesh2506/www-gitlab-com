---
layout: handbook-page-toc
title: "Global Upside (India) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to India based employees

All of the benefits listed below are administered and managed by [Global Upside](https://globalupside.com/). As part of the onboarding process Global Upside will reach out to team members in their first week to arrange setup and enrollment.  Should you have any questions, please contact:

| Email | Purpose |
| ------ | ------ |
| `hr@globalpeoservices.com` | Employment-related questions |
| `benefitsops@globalupside.com` | Questions regarding benefits elections |

## Medical Benefits

* Group Mediclaim Policy which will take care of hospitalization expenses, the coverage provided is available for the team member, their spouse and up to two children.  Should additional cover be required this will need to be purchased by the team member in the form of an Individual Policy. This can not be purchased under the Group Mediclaim Policy.
* Group Personal Accident policy including accidental death benefit.

## Pension

Global Upside has a provident fund that the employees pay to the government. This is included in the CTC. The Employees’ Provident Fund and Miscellaneous Provisions Act, 1952 (EPF Act) applies to specific scheduled factories and establishments with 20 or more employees, as well as employees who earn INR15,000 (INR25,000 for the disabled) or less per month at entry level. It ensures retirement benefits and family pensions on death in service. EPF benefits typically extend to all employees in an organization.


* Employee Contribution(% of base salary):
EPF: 3.67% and EPS: 8.33%, Total Contribution: 12%

* Employer Contribution(% of base salary):
EPF: 12%, EPS: None
Salary Ceiling: EPF: Mandatory contribution remains up to a monthly salary ceiling of INR15,000

## Life Insurance

Most companies in India do not offer life insurance as part of the benefits package. Global Upside, similarly, does not offer a life insurance plan to GitLab team members. Most workers in India will typically get their own life insurance which can be portable throughout their lifetime.

## Meal Vouchers

Sodexo Meal Cards are an optional employee purchased benefit. These Meal Cards work like a Debit Card, which can be used at any outlet selling food items and non-alcoholic beverages that accept card payments. If you would like purchase these a deduction from salary will be made each month. All team members are gien the option to opt-in for Sodexo cards during the pre-onboarding process managed by the CES (Candidate Experience Specialist) team.  

## GitLab B.V. India Leave Policy

* Statutory Maternity Leave
  - Women employees are eligible to claim 26 weeks paid maternity leave as per Maternity Benefit Act 1961. This can be availed maximum on two occasions only. A fitness certificate should be submitted at the time of resumption of duties.
  - Every women employee availing maternity leave is eligible for an insurance coverage of INR 50,000 for normal/C-section.
  - If employee wishes to extend her maternity leave, she shall make a written request to her Reporting Manager and Total Rewards Analysts with a valid justification. In such situation Reporting Manager and Total Rewards Analyst will review the case and inform employee to utilize her SL, CL and PLs for payable days OR / up to a maximum of 4 weeks from 90th day with loss of pay.

* Statutory Paternity Leave:  
  - All confirmed male employees are eligible for availing Paternity Leave not exceeding 3 consecutive working days (excluding holidays) up to 2 children. These leaves are paid leaves.

Total Rewards will consult with Global Upside to ensure that the statute is met.

## Gratuity

Gratuity is a statutory offering in India per The Payment of Gratuity Act, 1972 which is offered through Global Upside based on the Global Upside hire date. 

## Loyalty Bonus Scheme

GitLab has implemented a private loyalty bonus scheme since some team members have been contracted through different employment types as we have worked through country conversion processes. Therefore, the GitLab Loyalty Bonus Scheme will supplement gratuity based on the GitLab hire date vs the PEO hire date. 

**Elibility:** One can only claim Loyalty Bonus after retirement or resign from the service after completing 5 years of continuous service.

**Forfeiture of Loyalty Bonus:** The full amount of Loyalty Bonus can be forfeited if a team member’s services have been terminated due to: a) Riotous or disorderly conduct or any other violent act; b) Committing an offence involving moral turpitude.

**Calculation of Loyalty Bonus:**

Loyalty Bonus Amount =  Gratuity Calculation using GitLab Hire Date + Gratuity Calculation using PEO Hire Date

`The duration for Gratuity Calculation using GitLab Hire Date is the duration prior to the hire date of the PEO`

**Note:** The Loyalty Bonus Amount is provided by GitLab to the team members. Any amount which GitLab will pay as Loyalty Bonus would be fully taxable for the team members. 
