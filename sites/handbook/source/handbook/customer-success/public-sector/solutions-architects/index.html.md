---
layout: handbook-page-toc
title: "Public Sector Solutions Architects"
---

# Public Sector Solutions Architect Handbook
{:.no_toc}

Solutions Architects (SA) who work with the P[ublic Sector](/handbook/sales/public-sector/) provide subject matter expertise and industry experience throughout the sales process to Public Sector customers within the United States.

Because specific requirements and common engagement practices differ from Enterprise or Commercial customers, the guidance below exists to assist Solutions Architects who work with customers in the Public Sector specifically.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Federal Support

The GitLab Support Team provides [U.S.-based support](https://about.gitlab.com/support/#us-federal-support) for those customers that require U.S. citizens to work their support tickets.  
* The user that submits the ticket needs to be associated with the appropriate account in SFDC.  
* All accounts and user information gets synched between SFDC and Zendesk on an hourly bases under the conditions detailed on the [Support Ops page](https://about.gitlab.com/handbook/support/support-ops/responsibilities.html#sfdcus-federal-zendesk-sync). 
* All communications with support will be asynchronous, unless a synchronous call is requested by the Technical Account Manager and/or Solutions Architect for that account.
* Only customers designated as Public Sector in SFDC are eligible for Federal Support.


## Technical Close Plans

Technical Close Plans provide insight and transparency to the sales process by highlighting the technical ecosystem, competitive landscape, evaluation goals and technical steps required in order to achieve a technical win (Salesforce Stage 3). These plans are templated and only required for opportunities exceeding $100K in revenue.

To create a technical close plan:
* Log in to Salesforce and locate the relevant Opportunity
* Ensure the Opportunity is at stage 3 or is soon transitioning to stage 3
* Make a copy of the template document located in the Public Sector Google drive and rename it based on the customer and opportunity
* Complete the document fields with all known information
* Update the technical close plan with outcomes and next steps after each customer interaction

When the opportunity progresses to stage 4, the technical close plan is complete. A brief retrospective on the information helps the team identify trends in customer needs as well as clear paths to opportunity wins. In case of opportunity loss, a brief retrospective on the information can help populate the Closed Lost reason in Salesforce.

## Best Practices for writing RFx responses

Responding to a Request for _____ (RFx) is part of the standard process within Public Sector.  RFx is a general category that includes Request for Information, Request for Proposal, Request for Quote, etc. RFIs are generally less structured than RFPs. While RFQs rarely need technical write ups, occasionally technical input is required, especially if the RFx requests an `or alike` product.

### Responding to RFIs

RFIs are generally issued to shape procurement. In some situations, they are just a step in the process as the customer may already have an advising team that may be following protocol. An RFI is generally non-binding unless otherwise specified.  Responses to questions in an RFI don't have to be precise.  They can have some idealistic statements to lay the groundwork and pivot points to educate the customer on what we offer.  It is good to follow the GitLab Value Framework response methodology, as there is no ability to have a dialog. The RFI is about positioning. Provide context around the following factors that can grab the attention of the readers:

* Positioning and value of the product
* Truth about capabilities
* Solution options and flexibility
* How risk is mitigated by the solution

Some RFIs have a length limit, but be sure to verify and be mindful if there is one. Start with a simple overview of what the product does. GitLab Marketing does a great job with text that is vetted. For example, utilize the writeups for each of the [stages of the DevOps workflow](https://about.gitlab.com/stages-devops-lifecycle/). Do not reinvent the wheel, especially for an RFI. Keep it simple and succinct.  

An RFI may ask open-ended questions. This is good for providing detailed solution responses. Keep the language simple and describe the solution GitLab offers. Instead of stating that GitLab does or does not do something, direct the reader to why GitLab doesn't do it, if it is on the roadmap, or how they would implement a workaround.

When responding to an RFI, it's critical to document how the product solves customer problems, but it's also important to include the company behind the product. GitLab's all-remote leadership, its company values, its culture, and its professional services offerings shape the entire customer relationship.

### Including Links to Documentation
In general, for Public Sector responses, adding links to documentation is not a good practice. Essentially, anything that makes a reader have to do extra work is not going to work well. There are also cases in which the reader may not be able to readily access the link provided.

For example, if the question asks for a Roadmap for the next product release, it is a good idea to include a link to our roadmap, but then also explain GitLab's release velocity and consistency so the customer understands the dynamic nature of the GitLab release process. Another instance when links are desirable is when relevant customer use cases may be referenced. 

If, however, the customer asks for a descriptive concept like the architecture of GitLab, use that as a way to include as much detail as possible inline, a summary of why the architecture is what it is, including the benefits, and then a link for them to read more. Also, including images and screenshots is highly encouraged where possible. 

The customer may ask for a description of the CI process or other complex process. In these cases, it is acceptable to copy as much of the documentation from our documentation as possible, including many details to differentiate GitLab from competitive products. Using CI as an example, include a description of how GitLab CI operates, a description of the yml file, a link to the yml file documentation, and add information regarding unique or differentiating functions that GitLab offers like Auto DevOps, DAG, multi-project pipelines, etc.

### Tips and Tricks in Response Writing

* As much as possible use [active rather than passive voice](https://www.grammarly.com/blog/active-vs-passive-voice/)
* Eliminate pronouns: Example 1, "GitLab CI will do x" rather than "Our CI will do x". Example 2, "The GitLab team will x" rather than "We will x".
* Organization of the response is as important as the content
* Create a strong introduction and summary leveraging existing Marketing information which presents the breadth of the entire GitLab solution
* If specific requirements are expected to be answered by the response, add notations to the requirement number being met within the text of the response: Example, "GitLab's SAST scanner will analyze your source code for known vulnerabilities (Req 1.a.1)"
* Use customer terminology wherever possible
* Include relevant customer use cases whenever possible
