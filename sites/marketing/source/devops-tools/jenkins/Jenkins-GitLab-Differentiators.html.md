---
layout: markdown_page
title: "GitLab Over Jenkins Key Differentiators"
description: "GitLab provides more than what Jenkins is hoping to evolve to, by providing a fully integrated single application for the entire DevOps lifecycle."
canonical_path: "/devops-tools/jenkins/Jenkins-GitLab-Differentiators.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


## On this page
{:.no_toc}

- TOC
{:toc}

## Single Appication
* GitLab provides more than what Jenkins is hoping to evolve to, by providing a fully integrated single application for the entire DevOps lifecycle. More than Jenkins' goals, GitLab also provides planning, SCM, packaging, release, configuration, and monitoring (in addition to the CI/CD that Jenkins is focused on).  With Gitlab, there is no need for plugins and customization.  Unlike Jenkins, GitLab is [open core](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.

## Maintenance
* Maintaining GitLab is quite easy to maintain and update.  To upgrade, just replace one docker image
When you upgrade version, it upgrades everything.
* Maintaining Pipleline Definitions are cleaner and easier than in Jenkins.  The GitLab CI/CD configuration file  (.gitlab-ci.yml):
    * Uses a structured YAML format
    * Has a shorter learning curve - simpler to get up and running
    * Prevents unconstrained complexity - which can make your Jenkins pipeline files hard to understand and manage
    * Versioned in repository next to your code - makes it easy to maintain and update by developers

## Optimized for Cloud Native development  
* GitLab has 
    * Built-in container registry
    * Kubernetes integration
    * Microservices CI/CD logic
    * Cluster monitoring
    * Review Apps  

## Visiblity and Intuitiveness 
* Developers have visibility to the entire development lifecycle 
* Modern UX and ease of use

## Better execution architecture
* The GitLab Runner, which executes your CI/CD jobs, has an execution architecture that is more adaptable than Jenkins
    * Written in Go for portability - distributed as single binary without any other requirements.
    * Native Docker support - Jobs run in Docker containers.  GitLab Runner downloads the container images with the necessary tools, runs the jobs, then removes the container. 
    * No extra dev tool pre-installs needed on the Runner machines  
    * Built-in auto-scaling -  machines created on demand to match job demand. Enables cost savings by using resources more dynamically.

## Permission Model
* GitLab has a Single Permission Model which makes setting roles and permissions in GitLab faser and easier.
