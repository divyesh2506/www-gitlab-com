---
layout: markdown_page
title: "GitLab over CircleCI Key Differentiators"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## GitLab vs. CircleCI Analytics: Repo Insights

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Repo-Insights.png)

## GitLab vs. CircleCI Analytics: Per Project Insights

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Per-Project-Insights.png)

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Per-Project-Insights2.png)

## CircleCI CI SaaS Analytics

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/SaaS-Analytics.png)

## GitLab vs. CircleCI Dashboard: Viewing Pipelines

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Viewing-Pipelines.png)

## GitLab vs. CircleCI Dashboard: Viewing Job Logs

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Viewing-Job-Logs.png)

## GitLab vs. CircleCI Hybrid CI Orchestration

![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Hybrid-CI.png)

